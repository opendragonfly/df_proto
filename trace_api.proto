// SPDX-FileCopyrightText: Copyright 2022-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

syntax = "proto3";

package trace_api;

option go_package = "./trace_api";

enum SPAN_KIND {
  SPAN_KIND_UNSPECIFIED = 0;
  SPAN_KIND_INTERNAL = 1;
  SPAN_KIND_SERVER = 2;
  SPAN_KIND_CLIENT = 3;
  SPAN_KIND_PRODUCER = 4;
  SPAN_KIND_CONSUMER = 5;
}

enum SORT_BY {
  SORT_BY_START_TIME = 0;
  SORT_BY_CONSUME_TIME = 1;
}

message AttrInfo {
  string key = 1;
  string value = 2;
}

message EventInfo {
  string name = 1;
  int64 timeStamp = 2;
  repeated AttrInfo attrList = 3;
}

message SpanInfo {
  string spanId = 1;
  string parentSpanId = 2;
  string spanName = 3;
  int64 startTimeStamp = 4;
  int64 endTimeStamp = 5;
  SPAN_KIND spanKind = 6;
  repeated AttrInfo attrList = 7;
  repeated EventInfo eventList = 8;
}

message ServiceInfo {
  string serviceName = 1;
  string serviceVersion = 2;
}

message TraceInfo {
  string traceId = 1;
  ServiceInfo service = 2;
  SpanInfo rootSpan = 3;
}

//用于数据库保存数据
message TraceFullInfo {
  string traceId = 1;
  ServiceInfo service = 2;
  repeated SpanInfo spanList = 3;
}

//用于数据库保存数据
message TraceIdWithTime {
  string traceId = 1;
  int64 startTimeStamp = 2;
}

//用于数据库保存数据
message TraceIdWithTimeList {
  repeated TraceIdWithTime itemList = 1;
}

service TraceApi {
  //列出服务
  rpc listService(ListServiceRequest) returns (ListServiceResponse) {}
  //列出入口名称
  rpc listRootSpanName(ListRootSpanNameRequest)
      returns (ListRootSpanNameResponse) {}
  //列出Trace
  rpc listTrace(ListTraceRequest) returns (ListTraceResponse) {}
  //获得单个Trace
  rpc getTrace(GetTraceRequest) returns (GetTraceResponse) {}
  //列出Span
  rpc listSpan(ListSpanRequest) returns (ListSpanResponse) {}
}

message ListServiceRequest {
  string accessToken = 1;
  string remoteId = 2;
  int64 fromTime = 3;
  int64 toTime = 4;
}

message ListServiceResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_WRONG_TIME_RANGE = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  repeated ServiceInfo serviceList = 3;
}

message ListRootSpanNameRequest {
  string accessToken = 1;
  string remoteId = 2;
  ServiceInfo service = 3;
  int64 fromTime = 4;
  int64 toTime = 5;
}

message ListRootSpanNameResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_WRONG_TIME_RANGE = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  repeated string nameList = 3;
}

message ListTraceRequest {
  string accessToken = 1;
  string remoteId = 2;
  ServiceInfo service = 3;
  bool filterByRootSpanName = 4;
  string rootSpanName = 5;

  int64 fromTime = 10;
  int64 toTime = 11;

  uint32 limit = 20;
  SORT_BY sortBy = 21;
}

message ListTraceResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_WRONG_TIME_RANGE = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  repeated TraceInfo traceList = 3;
}

message GetTraceRequest {
  string accessToken = 1;
  string remoteId = 2;
  ServiceInfo service = 3;
  string traceId = 4;
}

message GetTraceResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_NO_TRACE = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  TraceInfo trace = 3;
}

message ListSpanRequest {
  string accessToken = 1;
  string remoteId = 2;
  ServiceInfo service = 3;
  string traceId = 4;
}

message ListSpanResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_NO_TRACE = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  repeated SpanInfo spanList = 3;
}